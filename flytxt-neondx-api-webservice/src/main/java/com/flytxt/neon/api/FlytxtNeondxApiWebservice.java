package com.flytxt.neon.api;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.flytxt.configuration.NeonDatabaseConfiguration;
@Import({ NeonDatabaseConfiguration.class })
@EnableJpaRepositories
@SpringBootApplication
public class FlytxtNeondxApiWebservice {


 	public static void main(String[] args) {
		SpringApplication.run(FlytxtNeondxApiWebservice.class, args);
	}
	

}
