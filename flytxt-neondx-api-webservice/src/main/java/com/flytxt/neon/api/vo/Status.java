package com.flytxt.neon.api.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import static com.flytxt.neon.api.constant.ApiConstant.STATUS;

/**
 * API Status
 */
@XmlRootElement(name = STATUS)
@XmlAccessorType(XmlAccessType.NONE)
public class Status {
	@XmlElement
	private int code;

	@XmlElement
	private String message;

	public Status() {
	}

	public Status(ApiStatus status) {
		this.code = status.getCode();
		this.message = status.getMessage();
	}

	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return "code=" + code + ",message=" + message;
	}

}
