package com.flytxt.neon.api.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import static com.flytxt.neon.api.constant.ApiConstant.*;

/**
 * A wrapper class representing a list of Event
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = EVENTS)
public class Events {

	@XmlElement(name = EVENT)
	private List<Event> events = new ArrayList<Event>();

	/**
	 * Add an Event
	 * 
	 * @param event
	 */
	public void add(Event event) {
		events.add(event);
	}

	/**
	 * @return the events
	 */
	public List<Event> getEvents() {
		return events;
	}

	@Override
	public String toString() {
		return events.toString();
	}

}
