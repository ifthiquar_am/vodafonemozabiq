package com.flytxt.neon.api.controller;

import static com.flytxt.neon.api.constant.ApiConstant.PATH_AUTH_KEY;
import static com.flytxt.neon.api.constant.ApiConstant.PATH_AUTH_KEY_PARAM;
import static com.flytxt.neon.api.constant.ApiConstant.PATH_EVENTS;
import static com.flytxt.neon.api.constant.ApiConstant.PATH_KPI;
import static com.flytxt.neon.api.constant.ApiConstant.PATH_MSISDN;
import static com.flytxt.neon.api.constant.ApiConstant.PATH_MSISDN_PARAM;

import java.net.ConnectException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/*import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;*/

import com.flytxt.neon.api.service.ApiService;
import com.flytxt.neon.api.service.OfferDetailsCache;
import com.flytxt.neon.api.vo.ApiResponse;
import com.flytxt.neon.api.vo.ApiStatus;
import com.flytxt.neon.api.vo.EventsApi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/neon-api-webservice/proxy-rest")
public class ApiController {
	@Autowired
	public ApiService apiService;
	
	@Autowired
	private OfferDetailsCache offerDetailsCache;
	
	
	
	
	@RequestMapping("/welcome")
	public ApiResponse getAllString() {
		ApiResponse response = new ApiResponse();
	   return response;
	   
	}

	@RequestMapping(value=PATH_AUTH_KEY+PATH_AUTH_KEY_PARAM+PATH_MSISDN+PATH_MSISDN_PARAM+PATH_KPI+PATH_EVENTS,method=RequestMethod.POST)
	public ApiResponse updateEvent(@RequestBody EventsApi events,@PathVariable String msisdn,@PathVariable String authkey) {
		log.info("Events {}",events.toString());
		
		log.debug(PATH_AUTH_KEY+PATH_AUTH_KEY_PARAM+PATH_MSISDN+PATH_MSISDN_PARAM+PATH_KPI+PATH_EVENTS);
		
		log.info("{} : {} : {} : {}",PATH_AUTH_KEY,authkey,PATH_MSISDN,msisdn);
		ApiResponse apiResponse = new ApiResponse(ApiStatus.SUCCESS);
		try {
			apiResponse= apiService.getApi(events,authkey,msisdn);
		} catch (ConnectException e) {
			apiResponse.setStatus(ApiStatus.SERVER_ERROR);
		}
		 return  apiResponse;  
		   
	}
	@RequestMapping("/cache/clear")
	public void clearCache()
	{
		offerDetailsCache.refreshAllOfferDetails();
	}
}







