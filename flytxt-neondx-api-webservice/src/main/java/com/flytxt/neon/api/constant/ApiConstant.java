package com.flytxt.neon.api.constant;

public interface ApiConstant {

	public static final String NAME = "name";

	public static final String TYPE = "type";

	public static final String SIZE = "size";

	public static final String MSISDN = "msisdn";

	public static final String TOUCHPOINT_ID = "touchpoint-id";

	public static final String QUERY = "query";

	public static final String QUERY_BALANCE = "balance";

	public static final String QUERY_LOYALTY_BALANCE = "loyalty-balance";

	public static final String AUTH_KEY = "authkey";

	public static final String RESPONSE = "response";

	public static final String CATEGORY = "category";

	public static final String CATEGORIES = "categories";

	public static final String SUB_CATEGORY = "sub-category";

	public static final String SUB_CATEGORIES = "sub-categories";

	public static final String ACTION = "action";

	public static final String ACTION_DEBIT = "debit";

	public static final String POINTS = "points";
	
	public static final String KEYS = "key";

	public static final String MESSAGE = "message";

	public static final String OFFER = "offer";

	public static final String OFFER_RECHARGE = "recharge-offer";

	public static final String OFFERS = "offers";

	public static final String OFFER_ID = "offerId";

	public static final String EXPIRY_TIME = "expiry-time";

	public static final String STATUS = "status";

	public static final String CC = "cc";

	public static final String EVENT = "event";

	public static final String EVENTS = "events";

	public static final String PROFILE = "profile";

	public static final String PROFILES = "profiles";

	public static final String HEADER_ACCEPT = "Accept";

	public static final String REDIRECT_AUTH_KEY = "FLY-API";

	public static final String PATH_AUTH_KEY = "/authkey";

	public static final String PATH_AUTH_KEY_PARAM = "/{authkey}";

	public static final String PATH_MSISDN = "/msisdn";

	public static final String PATH_MSISDN_PARAM = "/{msisdn}";

	public static final String PATH_OFFER_META = "/offer_meta";

	public static final String PATH_CATEGORIES = "/categories";

	public static final String PATH_SUB_CATEGORIES = "/sub_categories";

	public static final String PATH_OFFERS = "/offers";

	public static final String PATH_OFFERS_PARAM = "/{offerId}";

	public static final String PATH_KPI = "/kpi";

	public static final String PATH_LOYALTY = "/loyalty";

	public static final String PATH_SEND_SMS = "/send/sms";

	public static final String PATH_EVENTS = "/events";

	public static final String PATH_PROFILES = "/profiles";

	public static final String PATH_CACHES = "/caches";

	public static final String PATH_REFRESH_CACHES = "/refresh_caches";
	
	public static final String PATH_REFRESH = "/refresh";
	
	public static final String PATH_CLEAR = "/clear";
 
	public static final String CONS_PROFILE = "profile";

	public static final String CONS_DETAIL = "details";

	public static final String LISTS = "lists";

	public static final String CARRIER_DETAILS = "carrier";

	public static final String CARRIER_ID = "id";

	public static final String PROFILE_OPTIONS = "option";

	public static final String LIST = "list";

	public static final String DOUBLE_OPTIN = "double_optin";

	public static final String CONSUMER = "consumer";

	public static final String FIELD = "field";

	public static final String TEXT = "text";

	public static final String CONSUMERCARRIER = "consumer-carrier";

	public static final String KEYWORD = "keyword";

	public static final String TRIGGERAPPLICATION = "trigger-application";

	public static final String SOURCEADDRESS = "source-address";

	public static final String CHANNEL = "channel";

	public static final String TEXT1 = "text1";

	public static final String TEXT2 = "text2";

	public static final String TEXT3 = "text3";

	public static final String TEXT4 = "text4";

	public static final String TEXT5 = "text5";

	public static final String TEXT6 = "text6";

	public static final String TEXT7 = "text7";

	public static final String TEXT8 = "text8";

	public static final String TEXT9 = "text9";

	public static final String TEXT10 = "text10";

	public static final String INBOUNDUSSD = "inbound-ussd";

	public static final String SESSIONID = "session-id";

	public static final String PROMOTIONS = "promotions";

	public static final String PROMOTION = "promotion";

	public static final String OPTION = "option";

	public static final String PATH_CONSUMER_PROFILE = "/consumerprofile";

	public static final String USAGE_METRICS = "usageMetrics";

	public static final String USAGE_METRIC = "usageMetric";

	public static final String METRIC_VALUE = "metricValue";

	public static final String METRIC_HISTORY_NAME = "metricHistoryName";

	public static final String PATH_INBOUND_RESOURCE = "/receive";

	public static final String PATH_TRIGGER_APPLICATION = "/triggerapplication";

	public static final String PATH_SMS = "/sms";

	public static final String PATH_USSD = "/ussd";

	public static final String PATH_PROMOTION_DETAILS = "/promotiondetails";
	
	public static final String PATH_SPOKE_REQUEST = "spokeRequest";

	public static final String PATH_CHANGE_LOG_REQUEST = "changeLogRequest";
	
	public static final String PATH_HUB_CHANGE_ID_REQUEST = "hubChangeIdRequest";
	
	public static final String PATH_SPOKE_CHANGE_LOG_REQUEST = "spokeChangeLogRequest";
	
	public static final String PATH_HUB_SYNC_REQUEST = "hubSyncRequest";
	
	public static final String PATH_HUB_CHANGE_LOG_REQUEST = "hubChangeLogRequest";
	
	public static final String SPOKE_SUBMISSION_REQUEST = "spokeSubmissionRequest";

	public static final String HUB_SYNC_REQUEST = "hubSyncRequest";

	public static final String HUB_CHANGE_ID_REQUEST = "hubChangeIdRequest";
	
	public static final String CHANGE_LOG_REQUEST = "changeLogRequest";
	
	public static final String SYNC_MODE_REQUEST = "syncModeRequest";
	
	public static final String SPOKE_MODE_CHANGE_ID_REQUEST = "spokeModeChangeIdRequest";
	
	public static final String SPOKE_SUBMISSION_RESPONSE = "spokeSubmissionResponse";

	public static final String HUB_SYNC_RESPONSE = "hubSyncResponse";
	
	public static final String HUB_CHANGE_ID_RESPONSE = "hubChangeIdResponse";
	
	public static final String SPOKE_MODE_CHANGE_ID_RESPONSE = "spokeModeChangeIdResponse";
	
	public static final String CHANGE_LOG_RESPONSE = "changeLogResponse";
	
	public static final String SYNC_MODE_RESPONSE = "syncModeResponse";
	
	public static final String CAMPAIGN_LIB_HUB_INFO = "campaignLibraryHubInfo";
	
	public static final String URL_DELIMITER = "/";
	
	public static final String CCL = "/campaignLibrary";
	
	public static final String SYNC_SUCCESS = "Last Sync Successful at : ";
	
	public static final String SYNC_FAILURE = "Last Sync Falied at : ";
	
	public static final String SYNC_NOT_AUTHORISED = "Opco not Autorised and sync Failed at : ";

	public static final String RECHARGE_DATA = "recharge-data";

	public static final String SOURCE_ID = "source-id";
	
	public static final String BAD_REQUEST_EXCEPTION_MESSAGE = "HTTP 400 Bad Request";
	
	public static final String METHOD_NOT_ALLOWED_EXCEPTION_MESSAGE ="HTTP 405 Method Not Allowed";
	
	public static final String CACHE_REFRESH_SERVICE = "cacheRefreshService";
	
	public static final String SERVING_MANAGER_CACHE = "ServingManagerCache";
	
	public static final String TRACKING_API_CONFIG_CACHE = "TrackingAPIConfigCache";
	
	public static final String NEON_ENTITY_CACHE = "NeonEntityCache";
	
	public static final String OFFER_CACHE = "OfferCache";
	
	public static final String PLATFORM_SETTINGS_CACHE = "PlatformSettingsCache";
	
    public static final String MANDATORY = "Y";
	
	public static final String OFFER_MESSAGE = "offer-message";
	
	public static final String SESSION_ID = "session-id";
	
	public static final String INPUT = "input";
	
	public static final String NO_OFFER_TYPE = "nooffer";
	
	public static final String CONTINUE = "CONTINUE";
	
	public static final String RELEASE = "RELEASE";
	
	public static final String DATE = "recharge-date";
	
	public static final String TIMESTAMP = "Timestamp";

	public static final String PROGRAM_ID = "program-id";
	
	public static final String LOYALTY_DETAILS = "loyalty-details";
	
	public static final String LOYALTY_BALANCE = "loyalty-balance";
	
	public static final String LOYALTY_OFFERS = "loyalty-offers";
	
	public static final String LOYALTY_OFFER = "loyalty-offer";
	
	public static final String REDEMPTION_STATUS = "redemption-status";
	
	public static final String LOYALTY_OFFER_ID = "offer-id";
	
	public static final String BALANCE = "/balance";
	
	public static final String LOYALTY_RES_OFFERS = "/offers";
	
	public static final String REDEEM = "/redeem";
	
	public static final String LOYALTY_REWARD = "reward";
	
	public static final String REQUIRED_POINTS = "required-points";
	
	public static final String STATUS_CODE = "status-code";
	
	public static final String ADDITIONAL_PARAMS = "additional-params";
	
	public static final String PARAM1 = "param1";
	
	public static final String PARAM2 = "param2";
	
	public static final String ADDITIONAL_MESSAGES = "additional-messages";
	
	public static final String ADDITIONAL_MESSAGE = "additional-message";
	
	public static final String MESSAGE_LANGUAGE = "message-lang";
	
	public static final String MESSAGE_ENCODING = "message-encoding";
	
	public static final String MESSAGE_ENCODING_FORMAT = "UTF-8";
	
	public static final String USSD_MESSAGE = "ussd";
	
	public static final String SMARTCHARGE_MESSAGE = "smartCharge";
	
	public static final String CUSTOMER_CARE_MESSAGE = "customerCare";
	
	public static final String API_MESSAGE = "api";
	
	public static final String SMS_SINGLE_MESSAGE = "smsSingle";
	
	public static final String TID_PARAM="TID";
	
	public static final String REASON_PARAM="Reason";
			
}
