package com.flytxt.neon.api.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import static com.flytxt.neon.api.constant.ApiConstant.*;

/**
 * A wrapper class representing a list of Event
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = EVENTS)
public class EventsApi {

	@XmlElement(name = EVENT)
	private List<EventApi> events = new ArrayList<EventApi>();

	/**
	 * Add an Event
	 * 
	 * @param event
	 */
	public void add(EventApi event) {
		events.add(event);
	}

	/**
	 * @return the events
	 */
	public List<EventApi> getEvents() {
		return events;
	}

	@Override
	public String toString() {
		return events.toString();
	}
	

}
