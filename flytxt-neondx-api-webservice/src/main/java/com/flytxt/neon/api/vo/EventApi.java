package com.flytxt.neon.api.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import static com.flytxt.neon.api.constant.ApiConstant.*;

/**
 * This class represents Event
 */
@XmlRootElement(name = EVENT)
@XmlAccessorType(XmlAccessType.NONE)
public class EventApi {

	@XmlElement
	private String id;

	@XmlElement
	private String type;

	@XmlElement
	private String value;
	
	@XmlElement(name=TID_PARAM)
	private String tID;
	
	
	@XmlElement(name=REASON_PARAM)
	private String reason;

	@XmlElement
	private String date;

	/**
	 * Create a blank Event
	 */
	public EventApi() {
	}



	public EventApi(String id, String type, String value, String tID, String reason, String date) {
		super();
		this.id = id;
		this.type = type;
		this.value = value;
		this.tID = tID;
		this.reason = reason;
		this.date = date;
	}



	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}



	/**
	 * @return the tID
	 */
	public String gettID() {
		return tID;
	}



	/**
	 * @param tID the tID to set
	 */
	public void settID(String tID) {
		this.tID = tID;
	}



	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}



	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EventApi [id=" + id + ", type=" + type + ", value=" + value + ", tID=" + tID + ", reason=" + reason
				+ ", date=" + date + "]";
	}


	
}
