/**
 ******************************************************************************* Copyright (c)2007-2014 Flytxt Technology Pvt Ltd. All rights reserved. This software is the confidential and proprietary information of Flytxt
 * Technology Pvt Ltd. ("Confidential Information"). You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Flytxt.
 ******************************************************************************
 * @author shyju.thomas
 * @file ApiResponse.java
 * @version 1.0
 * @created Jun 11, 2014
 */
package com.flytxt.neon.api.vo;



import static com.flytxt.neon.api.constant.ApiConstant.RESPONSE;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * This class represents Response to Flytxt API Service. It wraps together various response elements like {@link Offers},
 * {@link OfferCategories} etc.
 */
@XmlRootElement(name =RESPONSE)
@XmlAccessorType(XmlAccessType.NONE)
public class ApiResponse {
	@XmlElement
	private Status status;

	
	public ApiResponse() {
		this(ApiStatus.SUCCESS);
	}

	public ApiResponse(ApiStatus status) {
		this.status = new Status(status);
	}

	public void setStatus(ApiStatus status) {
		this.status = new Status(status);
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Status getStatus() {
		return status;
	}

	@Override
	public String toString() {
		return status.toString();
	}
}
