/**
 * Filename: AppTid.java
 *
 * © Copyright 2015 Flytxt BV. ALL RIGHTS RESERVED.

 * All rights, title and interest (including all intellectual property rights) in this software and any derivative works based upon or derived from
 * this software belongs exclusively to Flytxt BV.

 * Access to this software is forbidden to anyone except current employees of Flytxt BV and its affiliated companies who have executed non-disclosure
 * agreements explicitly covering such access. While in the employment of Flytxt BV or its affiliate companies as the case may be, employees may use
 * this software internally, solely in the course of employment, for the sole purpose of developing new functionalities, features, procedures,
 * routines, customizations or derivative works, or for the purpose of providing maintenance or support for the software. Save as expressly permitted
 * above, no license or right thereto is hereby granted to anyone, either directly, by implication or otherwise. On the termination of employment,
 * the license granted to employee to access the software shall terminate and the software should be returned to the employer, without retaining any
 * copies.

 * This software is (i) proprietary to Flytxt BV; (ii) is of significant value to it; (iii) contains trade secrets of Flytxt BV; (iv) is not publicly
 * available; and (v) constitutes the confidential information of Flytxt BV.

 * Any use, reproduction, modification, distribution, public performance or display of this software or through the use of this software without the
 * prior, express written consent of Flytxt BV is strictly prohibited and may be in violation of applicable laws.
 *
 */
package com.flytxt.neon.api.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="app_tid",uniqueConstraints={@UniqueConstraint(columnNames={"tid_id"})})
public class AppTid {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="tid_id")
	 @GeneratedValue(strategy=GenerationType.AUTO)
	private Integer tidId;
	@NotNull
	private Long appInstanceId;
	@NotNull
	private String tid;
	@NotNull
	private int eventId;
	@NotNull
	private long consumerId;
	@NotNull
	private String reason;
	/**
	 * @return the tidId
	 */
	public Integer getTidId() {
		return tidId;
	}
	/**
	 * @param tidId the tidId to set
	 */
	public void setTidId(Integer tidId) {
		this.tidId = tidId;
	}
	/**
	 * @return the appInstanceId
	 */
	public Long getAppInstanceId() {
		return appInstanceId;
	}
	/**
	 * @param appInstanceId the appInstanceId to set
	 */
	public void setAppInstanceId(long appInstanceId) {
		this.appInstanceId = appInstanceId;
	}
	/**
	 * @return the tid
	 */
	public String getTid() {
		return tid;
	}
	/**
	 * @param tid the tid to set
	 */
	public void setTid(String tid) {
		this.tid = tid;
	}
	/**
	 * @return the eventId
	 */
	public int getEventId() {
		return eventId;
	}
	/**
	 * @param eventId the eventId to set
	 */
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	/**
	 * @return the consumerId
	 */
	public long getConsumerId() {
		return consumerId;
	}
	/**
	 * @param consumerId the consumerId to set
	 */
	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}
	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}
	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}
	public AppTid(int tidId, @NotNull long appInstanceId, @NotNull String tid, @NotNull int eventId,
			@NotNull long consumerId, @NotNull String reason) {
		super();
		this.tidId = tidId;
		this.appInstanceId = appInstanceId;
		this.tid = tid;
		this.eventId = eventId;
		this.consumerId = consumerId;
		this.reason = reason;
	}
	public AppTid() {
		super();
		// TODO Auto-generated constructor stub
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AppTid [tidId=" + tidId + ", appInstanceId=" + appInstanceId + ", tid=" + tid + ", eventId=" + eventId
				+ ", consumerId=" + consumerId + ", reason=" + reason + "]";
	}
	

}
