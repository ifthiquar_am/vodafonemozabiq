/**
 * Filename: ApiServiceImp.java
 *
 * © Copyright 2015 Flytxt BV. ALL RIGHTS RESERVED.

 * All rights, title and interest (including all intellectual property rights) in this software and any derivative works based upon or derived from
 * this software belongs exclusively to Flytxt BV.

 * Access to this software is forbidden to anyone except current employees of Flytxt BV and its affiliated companies who have executed non-disclosure
 * agreements explicitly covering such access. While in the employment of Flytxt BV or its affiliate companies as the case may be, employees may use
 * this software internally, solely in the course of employment, for the sole purpose of developing new functionalities, features, procedures,
 * routines, customizations or derivative works, or for the purpose of providing maintenance or support for the software. Save as expressly permitted
 * above, no license or right thereto is hereby granted to anyone, either directly, by implication or otherwise. On the termination of employment,
 * the license granted to employee to access the software shall terminate and the software should be returned to the employer, without retaining any
 * copies.

 * This software is (i) proprietary to Flytxt BV; (ii) is of significant value to it; (iii) contains trade secrets of Flytxt BV; (iv) is not publicly
 * available; and (v) constitutes the confidential information of Flytxt BV.

 * Any use, reproduction, modification, distribution, public performance or display of this software or through the use of this software without the
 * prior, express written consent of Flytxt BV is strictly prohibited and may be in violation of applicable laws.
 *
 */
package com.flytxt.neon.api.service;

import java.net.ConnectException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.flytxt.neon.api.entity.AppTid;
import com.flytxt.neon.api.entity.OfferDetails;
import com.flytxt.neon.api.repository.AppTidRepository;
import com.flytxt.neon.api.repository.OfferDetailsRepository;
import com.flytxt.neon.api.util.ApiHttpConnector;
import com.flytxt.neon.api.util.EventsTransfer;
import com.flytxt.neon.api.vo.ApiResponse;
import com.flytxt.neon.api.vo.ApiStatus;
import com.flytxt.neon.api.vo.EventApi;
import com.flytxt.neon.api.vo.Events;
import com.flytxt.neon.api.vo.EventsApi;

import lombok.extern.slf4j.Slf4j;

@Service
@PropertySource("classpath:application.yml")
@Slf4j
public class ApiServiceImp implements ApiService {

	@Autowired
	private AppTidRepository apiDao;

	@Autowired
	OfferDetailsRepository offerDetailsDao;
	@Autowired
	private OfferDetailsCache offerDetailsCache;

	EventsTransfer eventTransfer = new EventsTransfer();
	ApiHttpConnector apiHttpConn = new ApiHttpConnector();
	@Value("${neon-api.url}")
	private String url/* =env.getProperty("neon-api.url") */;

	public ApiResponse getApi(EventsApi _EventApi, String authkey, String msisdn) {
		log.info("getApi() :: EventApi : {} AuthKey : {} MSISDN : {}", _EventApi, authkey, msisdn);
		Events _Events = eventTransfer.getEvent(_EventApi);
		AppTid appTid;
		for (EventApi _Event : _EventApi.getEvents()) {
			appTid = new AppTid();
			appTid.setTid(_Event.gettID());
			appTid.setReason(_Event.getReason());
			appTid.setConsumerId(919341000004L);
			appTid.setAppInstanceId(
					offerDetailsCache.getKeyValue(Long.parseLong(_Event.getValue())).getAppInstanceId());
			apiDao.save(appTid);
		}
		String response;
		try {
			response = ApiHttpConnector.updateOffer(formUrl(url, authkey, msisdn), _Events);
		} catch (ConnectException e) {
			log.error("Connection refused {}",e.getMessage());
			response=":500,";
		}
		return setResponse(response);

	}

	private String formUrl(String url, String authkey, String msisdn) {
		url = url.replace("{authkey}", authkey);
		url = url.replace("{msisdn}", msisdn);
		return url;

	}

	@SuppressWarnings("unlikely-arg-type")
	private ApiResponse setResponse(String response) {
		ApiResponse apiResponse = new ApiResponse(ApiStatus.SERVER_ERROR);
		String responseCode = response.split(":")[2].split(",")[0];
		if (responseCode.equals(ApiStatus.SUCCESS)) {
			apiResponse.setStatus(ApiStatus.SUCCESS);
		} else if (responseCode.equals(ApiStatus.NO_MESSAGE)) {
			apiResponse.setStatus(ApiStatus.NO_MESSAGE);
		} else if (responseCode.equals(ApiStatus.BAD_REQUEST)) {
			apiResponse.setStatus(ApiStatus.BAD_REQUEST);
		} else if (responseCode.equals(ApiStatus.AUTH_FAILED)) {
			apiResponse.setStatus(ApiStatus.AUTH_FAILED);
		} else if (responseCode.equals(ApiStatus.INVALID_MSISDN)) {
			apiResponse.setStatus(ApiStatus.INVALID_MSISDN);
		} else if (responseCode.equals(ApiStatus.FAILED)) {
			apiResponse.setStatus(ApiStatus.FAILED);
		} else if (responseCode.equals(ApiStatus.SERVER_ERROR)) {
			apiResponse.setStatus(ApiStatus.SERVER_ERROR);
		}
		return apiResponse;
	}

}
