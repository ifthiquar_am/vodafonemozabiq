/**
 ******************************************************************************* Copyright (c)2007-2014 Flytxt Technology Pvt Ltd. All rights reserved. This software is the confidential and proprietary information of Flytxt
 * Technology Pvt Ltd. ("Confidential Information"). You shall not disclose such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Flytxt.
 ******************************************************************************
 * @author shyju.thomas
 * @file ApiStatus.java
 * @version 1.0
 * @created Jun 11, 2014
 */
package com.flytxt.neon.api.vo;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author shyju.thomas
 */
public enum ApiStatus {
	SUCCESS(200, "Success"),
	NO_OFFERS(205, "No offers are available"),
	NO_MESSAGE(210,"No message can be sent"),
	BAD_REQUEST(400, "Bad Request"),
	AUTH_FAILED(401, "Authentication Failed"),
	INVALID_MSISDN(404, "Invalid MSISDN"),
	FAILED(499, "Failed"),
	SERVER_ERROR(500, "Server Error");

	@XmlElement
	private int code;

	@XmlElement
	private String message;

	private ApiStatus(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
