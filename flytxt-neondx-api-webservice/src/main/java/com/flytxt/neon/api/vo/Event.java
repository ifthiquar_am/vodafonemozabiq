package com.flytxt.neon.api.vo;

import static com.flytxt.neon.api.constant.ApiConstant.EVENT;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class represents Event
 */
@SuppressWarnings("restriction")
@XmlRootElement(name = EVENT)
@XmlAccessorType(XmlAccessType.NONE)
public class Event {

	@XmlElement
	private String id;

	@XmlElement
	private String type;

	@XmlElement
	private String value;

	@XmlElement
	private String date;

	/**
	 * Create a blank Event
	 */
	public Event() {
	}

	/**
	 * Create a new Event with specified id, type, value and date
	 * 
	 * @param id
	 * @param type
	 * @param value
	 * @param date
	 */
	public Event(String id, String type, String value, String date) {
		this.id = id;
		this.type = type;
		this.value = value;
		this.date = date;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "id=" + id + ",type=" + type + ",value=" + value + ",date=" + date;
	}
}
